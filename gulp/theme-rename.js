module.exports = function( gulp, plugins, paths, config ) {
	return function() {
		return gulp.src( './wp-content/themes/customtheme/**/*' )
		           .pipe( gulp.dest( './wp-content/themes/' + config.name ) );
	};
};
