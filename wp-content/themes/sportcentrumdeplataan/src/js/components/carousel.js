/**
 * Carousel
 *
 * This component requires jQuery and Slick to function.
 * You can require it on the page using <div data-component="carousel"></div>
 */

define( [ 'jquery', 'slick' ], function( $ ) {
	$( function() {
		$( '[data-component="carousel"]' ).each( function() {
			var $this = $( this );

			$this.slick(
				{
					arrows: false,
					dots: true,
					slidesToShow: 1,
					infinite: true,
					rows: 0
				}
			);
		} );
	} );
} );