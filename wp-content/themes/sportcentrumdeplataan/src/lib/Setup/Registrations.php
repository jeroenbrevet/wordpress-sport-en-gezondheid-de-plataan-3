<?php

namespace Custom_Theme\Setup;

/**
 * Class Registrations
 *
 * Here are all registrations for custom post types
 * and custom taxonomies.
 *
 * @since      1.0
 *
 * @package    WordPress
 * @subpackage Custom_Theme\Setup
 */
class Registrations {
	/**
	 * Registrations constructor
	 *
	 * @since 1.0
	 */
	public function __construct() {
		add_action( 'init', [ $this, 'register_custom_post_types' ], 0 );
		add_action( 'init', [ $this, 'register_taxonomies' ], 0 );
	}

	/**
	 * Register theme custom post types
	 *
	 * @internal This function uses the `init` action
	 * @link     https://codex.wordpress.org/Plugin_API/Action_Reference/init
	 *
	 * @since    1.0
	 *
	 * @return void
	 */
	public function register_custom_post_types() {
		register_post_type( 'review',
			[
				'labels'             => [
					'name'          => __t( 'Reviews' ),
					'singular_name' => __t( 'Review' ),
					'menu_name'     => __t( 'Reviews' ),
				],
				'public'             => true,
				'publicly_queryable' => false,
				'menu_icon'          => 'dashicons-star-filled',
				'has_archive'        => false,
				'rewrite'            => [ 'slug' => __t( 'review' ) ],
				'supports'           => [
					'title',
					'editor',
				],
			]
		);

		register_post_type( 'medewerker',
			[
				'labels'             => [
					'name'          => __t( 'Medewerkers' ),
					'singular_name' => __t( 'Medewerker' ),
					'menu_name'     => __t( 'Medewerkers' ),
				],
				'public'             => true,
				'publicly_queryable' => false,
				'menu_icon'          => 'dashicons-groups',
				'has_archive'        => true,
				'rewrite'            => [ 'slug' => __t( 'medewerker' ) ],
				'supports'           => [
					'title',
					'thumbnail'
				],
			]
		);

		register_post_type( 'sport',
			[
				'labels'             => [
					'name'          => __t( 'Sports' ),
					'singular_name' => __t( 'Sport' ),
					'menu_name'     => __t( 'Sports' ),
				],
				'public'             => true,
				'publicly_queryable' => true,
				'menu_icon'          => 'dashicons-sos',
				'has_archive'        => true,
				'rewrite'            => [ 'slug' => __t( 'sport' ) ],
				'supports'           => [
					'title',
					'editor',
					'thumbnail'
				],
			]
		);
	}

	/**
	 * Register theme custom taxonomies
	 *
	 * @internal This function uses the `init` action
	 * @link     https://codex.wordpress.org/Plugin_API/Action_Reference/init
	 *
	 * @since    1.0
	 *
	 * @return void
	 */
	public function register_taxonomies() {
		register_taxonomy( 'medewerker_cat', [ 'medewerker' ],
			[
				'hierarchical'      => true,
				'labels'            => [
					'name'          => __t( 'Categories' ),
					'singular_name' => __t( 'Category' ),
					'menu_name'     => __t( 'Categories' ),
				],
				'show_ui'           => true,
				'show_admin_column' => true,
				'rewrite'           => [ 'slug' => __t( 'cat' ) ],
			]
		);

		register_taxonomy( 'sport_cat', [ 'sport' ],
			[
				'hierarchical'      => true,
				'labels'            => [
					'name'          => __t( 'Categories' ),
					'singular_name' => __t( 'Category' ),
					'menu_name'     => __t( 'Categories' ),
				],
				'show_ui'           => true,
				'show_admin_column' => true,
				'rewrite'           => [ 'slug' => __t( 'sport-categorie' ) ],
			]
		);
	}
}
