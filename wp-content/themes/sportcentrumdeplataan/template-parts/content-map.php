<?php
/**
 * The template part for displaying map content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
if ( ( $location = get_field( 'location' ) ) && ! empty( $location ) ) : ?>
	<div class="maps" data-zoom="12">
		<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
	</div>
<?php endif;
wp_enqueue_footer_script( 'maps' ); ?>
