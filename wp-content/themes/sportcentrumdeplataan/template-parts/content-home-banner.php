<?php
/**
 * The template part for displaying home banner
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */
$image  = get_field( 'banner_image' );
$title  = get_field( 'banner_title' );
$button = get_field( 'banner_button' );

if ( ! empty( $image ) || ! empty( $title ) || ! empty( $button ) ):?>
	<div class="banner banner--home" style="background-image: url(<?php echo $image['sizes']['home-banner']; ?>);">
		<div class="container">
			<?php if ( ! empty( $title ) ): ?>
				<h1><?php echo $title; ?></h1>
			<?php endif;

			if ( ! empty( $button ) ):?>
				<a href="<?php echo $button['url']; ?>" class="btn btn-success"><?php echo $button['title']; ?></a>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>

