// Import dependencies
var gulp = require( 'gulp' );
var plugins = require( 'gulp-load-plugins' )( { camelize: true } );
var config = require( './config.json' );
var del = require( 'del' );
var fileExists = require( 'file-exists' );
var merge = require( 'merge-stream' );
var node = require( './package.json' );
var runSequence = require( 'run-sequence' );

// Setup plugins
plugins.del = del;
plugins.fileExists = fileExists;
plugins.merge = merge;

// Additional configs
config.production = node.production;

// Development setup
if ( ! config.production ) {
	var browserSync = require( 'browser-sync' );

	plugins.browserSync = browserSync.create();

	if ( config.database.name !== '' ) {
		// Setup database connection
		var db = plugins.db(
			{
				user: config.database.user,
				password: config.database.password,
				host: config.database.hostname
			}
		);
	}
}

// Setup theme paths
var paths = {
	src: {
		css: './wp-content/themes/' + config.name + '/src/scss',
		fonts: './wp-content/themes/' + config.name + '/src/fonts',
		images: './wp-content/themes/' + config.name + '/src/img',
		js: './wp-content/themes/' + config.name + '/src/js',
		root: './wp-content/themes/' + config.name
	},
	dist: {
		css: './wp-content/themes/' + config.name + '/dist/css',
		fonts: './wp-content/themes/' + config.name + '/dist/fonts',
		images: './wp-content/themes/' + config.name + '/img',
		js: './wp-content/themes/' + config.name + '/dist/js',
		root: './wp-content/themes/' + config.name + '/dist'
	}
};

function getTask( task ) {
	return require( './gulp/' + task )( gulp, plugins, paths, config );
}

// Setup default tasks
gulp.task( 'clean', getTask( 'clean' ) );
gulp.task( 'composer:install', getTask( 'composer-install' ) );
gulp.task( 'composer:update', getTask( 'composer-update' ) );
gulp.task( 'fonts', getTask( 'fonts' ) );
gulp.task( 'images', getTask( 'images' ) );
gulp.task( 'project:gitignore', getTask( 'project-gitignore' ) );
gulp.task( 'project:robots', getTask( 'project-robots' ) );
gulp.task( 'scripts', getTask( 'scripts' ) );
gulp.task( 'styles', getTask( 'styles' ) );
gulp.task( 'wp:config', getTask( 'wp-config' ) );
gulp.task( 'wp:config-local', getTask( 'wp-config-local' ) );
gulp.task( 'wp:index', getTask( 'wp-index' ) );

// Setup development tasks
if ( ! config.production ) {
	gulp.task( 'composer:cleanup', getTask( 'composer-cleanup' ) );

	if ( config.database.name !== '' ) {
		gulp.task( 'database:create', db.create( config.database.name ) );
	}

	gulp.task( 'reload', getTask( 'reload' ) );
	gulp.task( 'theme:cleanup', getTask( 'theme-cleanup' ) );
	gulp.task( 'theme:info', getTask( 'theme-info' ) );
	gulp.task( 'theme:rename', getTask( 'theme-rename' ) );
	gulp.task( 'wp:salts', getTask( 'wp-salts' ) );

	// Compile and serve files
	gulp.task(
		'serve', [ 'styles', 'scripts', 'fonts', 'images' ], function() {
			plugins.browserSync.init(
				{
					notify: {
						styles: {
							top: 'auto',
							bottom: '0'
						}
					},
					port: 3000,
					proxy: config.url
				}
			);

			gulp.watch( '**/*.{scss,css}', { cwd: paths.src.css }, [ 'styles' ] );
			gulp.watch( '**/*.js', { cwd: paths.src.js }, [ 'scripts', 'reload' ] );
			gulp.watch( '**/*', { cwd: paths.src.fonts }, [ 'fonts', 'reload' ] );
			gulp.watch( '**/*', { cwd: paths.src.images }, [ 'images', 'reload' ] );
			gulp.watch( '**/*.php', { cwd: paths.src.root }, [ 'reload' ] );
			gulp.watch( './composer.json', [ 'composer:update', 'reload' ] );
		}
	);
}

// Compile files and launch development area, the default task
gulp.task(
	'default', function( callback ) {
		if ( ! config.production ) {
			return runSequence(
				'clean',
				'serve',
				callback
			);
		} else {
			return runSequence(
				'deploy',
				callback
			);
		}
	}
);

// Build package
gulp.task(
	'build', function( callback ) {
		if ( ! config.production ) {
			return runSequence(
				'project:gitignore',
				'project:robots',
				'composer:install',
				'composer:cleanup',
				'theme:rename',
				'theme:info',
				'theme:cleanup',
				'wp:config',
				'wp:config-local',
				'wp:index',
				'wp:salts',
				'database:create',
				'default',
				callback
			);
		} else {
			return runSequence(
				'project:gitignore',
				'project:robots',
				'composer:install',
				'wp:config',
				'wp:config-local',
				'wp:index',
				'deploy',
				callback
			);
		}
	}
);

// Deploy production files
gulp.task(
	'deploy', function( callback ) {
		return runSequence(
			'clean',
			[ 'styles', 'scripts', 'fonts', 'images' ],
			callback
		);
	}
);
